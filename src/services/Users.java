package services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import database.DatabaseInterface;

@Path("users")
public class Users {

	public Users() {
	}

	@GET
	@Produces("application/json")
	public String getUsers() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();

		String[] names = dbi.findAllDisplayNames();

		if (names != null) {
			try {
				JSONObject jo = new JSONObject();
				jo.put("users", new JSONArray(names));

				return jo.toString();
			} catch (JSONException e) {
				throw new WebApplicationException(
						Response.Status.INTERNAL_SERVER_ERROR);
			}
		}

		throw new WebApplicationException(Response.Status.BAD_REQUEST);
	}
}
