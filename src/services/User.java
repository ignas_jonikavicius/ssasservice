package services;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import database.DatabaseInterface;

@Path("/users/{user}")
public class User {

	@GET
	@Produces("application/json")
	public String getUser(@PathParam("user") String displayName) {

		DatabaseInterface dbi = DatabaseInterface.getInstance();
		String[] u = dbi.findUser(displayName);

		if (u != null) {
			// User exists
			JSONObject jo = new JSONObject();
			try {
				jo.put("nick", displayName);
				jo.put("hobbies", new JSONArray(new String[] { u[0] }));

				// Check relationships
				ArrayList<String[]> r = dbi.findRelationships(Integer
						.parseInt(u[1]));
				JSONArray ja = new JSONArray();
				for (String[] s : r) {
					JSONObject joR = new JSONObject();
					joR.put("nick", s[0]);
					joR.put("relationtype", s[1]);
					ja.put(joR);
				}
				jo.put("relationships", ja);

				return jo.toString();
			} catch (JSONException e) {
				throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
			}
		}

		throw new WebApplicationException(Response.Status.BAD_REQUEST);
	}
}
