package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseInterface {

	// server
	private String sqluser = "webapp";
	private String sqlpw = "E19928hu";
	private String databaseName = "SAS";

	private static DatabaseInterface instance;
	private DbMapper mapper;

	public DatabaseInterface() {
		this.mapper = new DbMapper();
	}

	public static DatabaseInterface getInstance() { // singleton
		if (instance == null) {
			instance = new DatabaseInterface();
		}
		return instance;
	}

	private Connection openDataBase() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ databaseName, sqluser, sqlpw);
		} catch (SQLException e) {
			System.err.println(e);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DatabaseInterface.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		return con;
	}

	private void close(Connection con) {
		try {
			con.close();
		} catch (SQLException e) {
			System.err.println(e);
		}
	}

	//
	// DB Calls
	//

	public String[] findAllDisplayNames() {
		Connection con = null;
		String[] names = new String[0];
		try {
			con = openDataBase();
			names = mapper.findAllDisplayNames(con);
		} finally {
			close(con);
		}
		return names;
	}

	public String[] findUser(String displayName) {
		Connection con = null;
		String[] users = null;
		try {
			con = openDataBase();
			users = mapper.findUser(displayName, con);
		} finally {
			close(con);
		}

		return users;
	}

	public ArrayList<String[]> findRelationships(int userId) {
		Connection con = null;
		ArrayList<String[]> relationships = null;
		try {
			con = openDataBase();
			relationships = mapper.findRelationships(userId, con);
		} finally {
			close(con);
		}

		return relationships;
	}
}
