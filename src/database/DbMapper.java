package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class DbMapper {

	public static final String USERS = "users";
	public static final String USER_RELATION = "user_relation";

	/**
	 * Return list of all users display names
	 */
	public String[] findAllDisplayNames(Connection con) {
		String SQLselect = String
				.format("SELECT display_name FROM %s WHERE role_code != 'ADMIN' ORDER BY display_name ASC",
						USERS);

		PreparedStatement getStatement = null;
		ResultSet resultSet = null;
		ArrayList<String> arrayList = new ArrayList<String>();

		try {
			getStatement = con.prepareStatement(SQLselect);

			resultSet = getStatement.executeQuery();
			while (resultSet.next()) {
				arrayList.add(resultSet.getString("display_name"));
			}

			return arrayList.toArray(new String[0]);
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - getUser(email)");
			System.out.println(e.getMessage());
		}

		return null;
	}

	/**
	 * Find User by display name. Returns User's hobbies if user exists
	 */
	public String[] findUser(String displayName, Connection con) {
		String SQLselect = String
				.format("SELECT hobbies, id FROM %s WHERE role_code != 'ADMIN' AND display_name=? LIMIT 1",
						USERS);

		PreparedStatement getStatement = null;
		ResultSet resultSet = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setString(1, displayName);

			resultSet = getStatement.executeQuery();
			if (resultSet.first()) {
				String[] u = new String[2];
				u[0] = (resultSet.getString("hobbies") == null ? "" : resultSet
						.getString("hobbies"));
				u[1] = resultSet.getString("id");
				return u;
			}

		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - getUser(email)");
			System.out.println(e.getMessage());
		}

		return null;
	}

	/**
	 * Find Relationships
	 */
	public ArrayList<String[]> findRelationships(int userId, Connection con) {
		String SQLselect = String
				.format("SELECT u.display_name, ur.romance FROM %s ur JOIN %s u ON (ur.fid = u.id) WHERE ur.uid = ?  ORDER BY u.display_name ASC",
						USER_RELATION, USERS);

		PreparedStatement getStatement = null;
		ResultSet resultSet = null;
		ArrayList<String[]> arrayList = new ArrayList<>();

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setInt(1, userId);

			resultSet = getStatement.executeQuery();
			while (resultSet.next()) {
				arrayList.add(new String[] {
						resultSet.getString("display_name"),
						(resultSet.getInt("romance") == 0 ? "FRIEND"
								: "ROMANCE") });
			}

			return arrayList;
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - getUser(email)");
			System.out.println(e.getMessage());
		}

		return null;
	}
}
